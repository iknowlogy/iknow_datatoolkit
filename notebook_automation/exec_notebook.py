import nbformat
import sys
import os

os.chdir(sys.argv[1])

from nbconvert.preprocessors import ExecutePreprocessor
notebook_filename = sys.argv[2]
nb = nbformat.read(open(notebook_filename), as_version=4)
ep = ExecutePreprocessor(timeout=60000)
ep.preprocess(nb, {'metadata': {'path': sys.argv[1] }})