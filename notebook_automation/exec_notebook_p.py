import nbformat
import sys
import os

params = sys.argv[3]
os.chdir(sys.argv[1])

from nbconvert.preprocessors import ExecutePreprocessor
notebook_filename = sys.argv[2]
nb = nbformat.read(open(notebook_filename), as_version=4)

first_cell_source = nb['cells'][0]['source']
modified_first_cell_source = first_cell_source.replace('INPUT_PARAMS_FROM_EXECUTOR', params) 
nb['cells'][0]['source'] = modified_first_cell_source



ep = ExecutePreprocessor(timeout=60000)
ep.preprocess(nb, {'metadata': {'path': sys.argv[1] }})