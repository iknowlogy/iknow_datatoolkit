def table_to_html(colnames_list, data):
    table_as_html = '<table border="1"><tr bgcolor="#c2ebed">'+(''.join(['<th>'+str(i)+'</th>' for i in colnames_list]))+'</tr>'
    for row in data:
        table_as_html += '<tr>'+(''.join(['<td>'+str(col)+'</td>' for col in row]))+'</tr>'
    table_as_html += '</table>'
    return table_as_html
	
	
	
def send_email(recepient_list,
               sender_email,
               subject,
               email_password,
               from_name,
               message_html,
			   images):
    
    import smtplib

    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.header import Header
    from email.utils import formataddr
    from email.mime.image import MIMEImage

    # me == my email address
    # you == recipient's email address
    me = sender_email
    you = ', '.join(recepient_list)

    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    #msg['From'] = me
    msg['From'] = formataddr((str(Header(from_name, 'utf-8')), me))
    msg['To'] = you

    # Create the body of the message (a plain-text and an HTML version).
    text = "HTML message was not loaded, displaying this simple text message instead :( "
    html = message_html

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)
    msg.add_header('reply-to', 'norelpy@iknowlogy.com')
    
    for image in images:
        with open(image['file'], 'rb') as fp:
            msgImage = MIMEImage(fp.read())
        msgImage.add_header('Content-ID', image['tag'])
        msg.attach(msgImage)

    # Send the message
    s = smtplib.SMTP('smtp.gmail.com:587')
    s.ehlo()
    s.starttls()
    s.login(me, email_password)

    # sendmail function takes 3 arguments: sender's address, recipient's address
    # and message to send - here it is sent as one string.

    s.sendmail(me, msg["To"].split(","), msg.as_string())
    s.quit()
    
    
def send_email_3(recepient_list,
               sender_email,
               subject,
               email_password,
               from_name,
               message_html,
			   images):
    
    import smtplib

    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.header import Header
    from email.utils import formataddr
    from email.mime.image import MIMEImage

    # me == my email address
    # you == recipient's email address
    me = sender_email
    you = ', '.join(recepient_list)

    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    #msg['From'] = me
    msg['From'] = formataddr((str(Header(from_name, 'utf-8')), me))
    msg['To'] = you

    # Create the body of the message (a plain-text and an HTML version).
    text = "HTML message was not loaded, displaying this simple text message instead :( "
    html = message_html

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)
    msg.add_header('reply-to', 'norelpy@iknowlogy.com')
    
    for image in images:
        with open(image['file'], 'rb') as fp:
            msgImage = MIMEImage(fp.read())
        msgImage.add_header('Content-ID', image['tag'])
        msg.attach(msgImage)

    # Send the message
    s = smtplib.SMTP('smtp.gmail.com:587')
    s.ehlo()
    s.starttls()
    s.login(me, email_password)

    # sendmail function takes 3 arguments: sender's address, recipient's address
    # and message to send - here it is sent as one string.

    s.sendmail(me, msg["To"].split(","), msg.as_string())
    s.quit()
	
	
	
def send_email_ses(recepient_list,
                   sender_email,
                   subject,
                   email_password,
                   email_user,
                   from_name,
                   message_html,
                   images=[],
                   attachments=[],
                   bcc = False
                  ):
    
    import smtplib

    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.header import Header
    from email.utils import formataddr
    from email.mime.image import MIMEImage
    from email.mime.base import MIMEBase
    from email import encoders

    # me == my email address
    # you == recipient's email address
    me = sender_email
    you = ', '.join(recepient_list)

    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    #msg['From'] = me
    msg['From'] = formataddr((str(Header(from_name, 'utf-8')), me))
    
    if bcc:
        msg['Bcc'] = you
    else:
        msg['To'] = you

    # Create the body of the message (a plain-text and an HTML version).
    text = "HTML message was not loaded, displaying this simple text message instead :( "
    html = message_html

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)
    msg.add_header('reply-to', 'norelpy@iknowlogy.com')
    
    for image in images:
        with open(image['file'], 'rb') as fp:
            msgImage = MIMEImage(fp.read())
        msgImage.add_header('Content-ID', image['tag'])
        msg.attach(msgImage)

    for attachment in attachments:
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(attachment['file'], "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="'+attachment['file_short_name']+'"')
        msg.attach(part)

    # Send the message
    # s = smtplib.SMTP('email-smtp.us-east-1.amazonaws.com:465')
    s = smtplib.SMTP_SSL('email-smtp.us-east-1.amazonaws.com',465)
    s.ehlo()
    # s.starttls()
    s.login(email_user, email_password)

    # sendmail function takes 3 arguments: sender's address, recipient's address
    # and message to send - here it is sent as one string.

    s.sendmail(me, you.split(","), msg.as_string())
    s.quit()