#!/bin/sh

CONT=-1
COUNTER=0

cd $3

while [ $CONT -ne 0 ]; do
 echo "$1"
        $2/gdrive-linux-x64 export $1 --force
        CONT=$?
        sleep 0.5
        ((COUNTER++))
        if [ $COUNTER -gt 40 ]; then
                echo "Tried too many times!!!"
                exit -1
        fi
done
