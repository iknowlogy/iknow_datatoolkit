import stripe
import gzip
import time
import datetime
import json

def get_stripe_charges(output_file,start_dt,end_dt,token):
    stripe.api_key = token
    batch_size = 100
    f = gzip.open(output_file, 'wt') 

    try:
        print('Starting '+str(start_dt))

        from_dt_ts = time.mktime(start_dt.timetuple())
        to_dt_ts = time.mktime(end_dt.timetuple())
        print('Loading from '+str(int(from_dt_ts))+' to '+str(int(to_dt_ts)))

        cont = True
        counter = 0
        while cont:
            counter += 1
            if counter == 1:
                res = stripe.Charge.list(limit=batch_size,created={'gte':int(from_dt_ts),'lte':int(to_dt_ts)})
            else:
                res = stripe.Charge.list(limit=batch_size,created={'gte':int(from_dt_ts),'lte':int(to_dt_ts)},starting_after=starting_after_id)
            cont = res['has_more']
            res_length = len(res['data'])
            if res_length==0:
                continue
            starting_after_id = res['data'][res_length-1]['id']
            last_created = res['data'][res_length-1]['created']
            new_list = []
            for row in res['data']:
                row['ik_ts'] = time.time()
                f.write(json.dumps(row))
                f.write('\n')

    finally:
        f.close()



def get_stripe_customers(output_file,start_dt,end_dt,token):
    stripe.api_key = token
    batch_size = 100
    f = gzip.open(output_file, 'wt') 

    try:
        print('Starting '+str(start_dt))

        from_dt_ts = time.mktime(start_dt.timetuple())
        to_dt_ts = time.mktime(end_dt.timetuple())
        print('Loading from '+str(int(from_dt_ts))+' to '+str(int(to_dt_ts)))

        cont = True
        counter = 0
        while cont:
            counter += 1
            if counter == 1:
                res = stripe.Customer.list(limit=batch_size,created={'gte':int(from_dt_ts),'lte':int(to_dt_ts)})
            else:
                res = stripe.Customer.list(limit=batch_size,created={'gte':int(from_dt_ts),'lte':int(to_dt_ts)},starting_after=starting_after_id)
            cont = res['has_more']
            res_length = len(res['data'])
            if res_length==0:
                continue
            starting_after_id = res['data'][res_length-1]['id']
            last_created = res['data'][res_length-1]['created']
            new_list = []
            for row in res['data']:
                row['ik_ts'] = time.time()
                for i,rr in enumerate(row['subscriptions']['data']):
                    row['subscriptions']['data'][i]['canceled_at']=0
                row['discount']=str(row['discount'])
                f.write(json.dumps(row))
                f.write('\n')

    finally:
        f.close()
		
		
def get_stripe_refunds(output_file,start_dt,end_dt,token):
    stripe.api_key = token
    batch_size = 100
    f = gzip.open(output_file, 'wt') 

    try:
        print('Starting '+str(start_dt))

        from_dt_ts = time.mktime(start_dt.timetuple())
        to_dt_ts = time.mktime(end_dt.timetuple())
        print('Loading from '+str(int(from_dt_ts))+' to '+str(int(to_dt_ts)))

        cont = True
        counter = 0
        while cont:
            counter += 1
            if counter == 1:
                res = stripe.Refund.list(limit=batch_size,created={'gte':int(from_dt_ts),'lte':int(to_dt_ts)})
            else:
                res = stripe.Refund.list(limit=batch_size,created={'gte':int(from_dt_ts),'lte':int(to_dt_ts)},starting_after=starting_after_id)
            cont = res['has_more']
            res_length = len(res['data'])
            if res_length==0:
                continue
            starting_after_id = res['data'][res_length-1]['id']
            last_created = res['data'][res_length-1]['created']
            new_list = []
            for row in res['data']:
                row['ik_ts'] = time.time()
                f.write(json.dumps(row))
                f.write('\n')

    finally:
        f.close()
		
def get_stripe_invoices(output_file,start_dt,end_dt,token):
    stripe.api_key = token
    batch_size = 100
    f = gzip.open(output_file, 'wt') 

    try:
        print('Starting '+str(start_dt))

        from_dt_ts = time.mktime(start_dt.timetuple())
        to_dt_ts = time.mktime(end_dt.timetuple())
        print('Loading from '+str(int(from_dt_ts))+' to '+str(int(to_dt_ts)))

        cont = True
        counter = 0
        while cont:
            counter += 1
            if counter == 1:
                res = stripe.Invoice.list(limit=batch_size,date={'gte':int(from_dt_ts),'lte':int(to_dt_ts)})
            else:
                res = stripe.Invoice.list(limit=batch_size,date={'gte':int(from_dt_ts),'lte':int(to_dt_ts)},starting_after=starting_after_id)
            cont = res['has_more']
            res_length = len(res['data'])
            if res_length==0:
                continue
            starting_after_id = res['data'][res_length-1]['id']
            last_created = res['data'][res_length-1]['date']
            if counter % 5 == 0:
                print(last_created)
            new_list = []
            for row in res['data']:
                row['ik_ts'] = time.time()
                f.write(json.dumps(row))
                f.write('\n')

    finally:
        f.close()
		
		
		
def get_stripe_fees(output_file,start_dt,end_dt,token):
    stripe.api_key = token
    batch_size = 100
    f = gzip.open(output_file, 'wt') 

    try:
        print('Starting '+str(start_dt))

        from_dt_ts = time.mktime(start_dt.timetuple())
        to_dt_ts = time.mktime(end_dt.timetuple())
        print('Loading from '+str(int(from_dt_ts))+' to '+str(int(to_dt_ts)))

        cont = True
        counter = 0
        while cont:
            counter += 1
            if counter == 1:
                res = stripe.BalanceTransaction.list(limit=batch_size,created={'gte':int(from_dt_ts),'lte':int(to_dt_ts)})
            else:
                res = stripe.BalanceTransaction.list(limit=batch_size,created={'gte':int(from_dt_ts),'lte':int(to_dt_ts)},starting_after=starting_after_id)
            cont = res['has_more']
            res_length = len(res['data'])
            if res_length==0:
                continue
            starting_after_id = res['data'][res_length-1]['id']
            last_created = res['data'][res_length-1]['created']
            if counter % 5 == 0:
                print(last_created)
            new_list = []
            for row in res['data']:
                row['ik_ts'] = time.time()
                f.write(json.dumps(row))
                f.write('\n')

    finally:
        f.close()
		
		
		
def get_stripe_disputes(output_file,start_dt,end_dt,token):
    stripe.api_key = token
    batch_size = 100
    f = gzip.open(output_file, 'wt') 

    try:
        print('Starting '+str(start_dt))

        from_dt_ts = time.mktime(start_dt.timetuple())
        to_dt_ts = time.mktime(end_dt.timetuple())
        print('Loading from '+str(int(from_dt_ts))+' to '+str(int(to_dt_ts)))

        cont = True
        counter = 0
        while cont:
            counter += 1
            if counter == 1:
                res = stripe.Dispute.list(limit=batch_size,created={'gte':int(from_dt_ts),'lte':int(to_dt_ts)})
            else:
                res = stripe.Dispute.list(limit=batch_size,created={'gte':int(from_dt_ts),'lte':int(to_dt_ts)},starting_after=starting_after_id)
            cont = res['has_more']
            res_length = len(res['data'])
            if res_length==0:
                continue
            starting_after_id = res['data'][res_length-1]['id']
            last_created = res['data'][res_length-1]['created']
            if counter % 5 == 0:
                print(last_created)
            new_list = []
            for row in res['data']:
                row['ik_ts'] = time.time()
                f.write(json.dumps(row))
                f.write('\n')

    finally:
        f.close()