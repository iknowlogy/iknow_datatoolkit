import snowflake.connector
import os
from google.cloud import bigquery

def test():
	print("Hi!!")
	
	
def load_json_file(path):
	import json
	with open(path) as myfile:
		return json.loads(myfile.read())
		
def load_file_to_string(path):
	import json
	with open(path) as myfile:
		return myfile.read().replace('\n',' ')
		
		
def snowflake_connect(connection_json):
    if connection_json['region']=='us-west-1':        
        con = snowflake.connector.connect(user=connection_json['user'],
                                          password=connection_json['password'],
                                          account=connection_json['account'],
                                          warehouse=connection_json['warehouse'],
                                          database = connection_json['database'],
                                          role= connection_json['role'],
                                          insecure_mode=True)
        
    else:
        con = snowflake.connector.connect(user=connection_json['user'],
                                          password=connection_json['password'],
                                          account=connection_json['account'],
                                          warehouse=connection_json['warehouse'],
                                          database = connection_json['database'],
                                          role= connection_json['role'],
                                          region = connection_json['region'],
                                          insecure_mode=True)
    
    return con
	
	
def snowflake_run_select(connection_json,
                         sql):
    con = snowflake_connect(connection_json)
    try:
        cur = con.cursor()
        cur.execute(sql)
        colnames_list = [x[0] for x in cur.description]
        data = cur.fetchall()
    finally:
        cur.close()
        con.close()

    return colnames_list,data
	
	
def azure_sqldb_run_select(creds,sql):
	
	import jaydebeapi
	conn = jaydebeapi.connect(creds['VERBIT']['BI_DB_HOST'],
							  creds['VERBIT']['BI_DB_URL'],
							  [creds['VERBIT']['BI_DB_USER'],creds['VERBIT']['BI_DB_PASS']],
							  creds['VERBIT']['BI_DB_PATHTOJDBC_DOCKER'],) #CHANGE SQLSERVER CON TO GENERIC

	curs = conn.cursor()
	curs.execute(sql)
	colnames = curs.description
	colnames_list = [i[0] for i in colnames]

	data = curs.fetchall()
	curs.close()
	conn.close()
	
	return colnames_list, data
	
	
	
def bq_auto_create_table_from_local_file(path_to_key,project_id,dataset_id,table_id,source_file,write_disposition,sw_autodetect=True):
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"]=path_to_key
    client = bigquery.Client(project=project_id)
    dataset_ref = client.dataset(dataset_id)    
    table_ref = dataset_ref.table(table_id)

    job_config = bigquery.LoadJobConfig()
    job_config.autodetect = sw_autodetect
    job_config.source_format = bigquery.SourceFormat.NEWLINE_DELIMITED_JSON
    job_config.write_disposition = write_disposition


    with open(source_file, 'rb') as source_file:
        job = client.load_table_from_file(
            source_file,
            table_ref,
            job_config=job_config)  # API request

    try:
        job.result()  # Waits for table load to complete.

        print('Loaded {} rows into {}:{}.'.format(
            job.output_rows, dataset_id, table_id))
            
    finally:
        return job
        
        
def bq_run_sql(path_to_key,project_id,sql):
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"]=path_to_key
    client = bigquery.Client(project=project_id)

    # Perform a query.
    query_job = client.query(sql)  # API request
    rows = query_job.result()  # Waits for query to finish
    return rows